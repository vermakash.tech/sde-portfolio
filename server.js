let express = require("express"),
    fs = require("fs"),
    mongoose = require("mongoose"),
    ejs = require("ejs"),
    sendMail = require("./js/email"),
    flash = require('connect-flash'),
    endrsmnt = require("./js/endorsementSchema"),
    session = require('express-session'),
    loadData = require("./js/retrieve"),
    dotenv = require('dotenv');

let app = express();

dotenv.config()
const dbURL = process.env.DB_URL
mongoose.connect(dbURL, {useUnifiedTopology: true, useNewUrlParser: true});

app.use(session({ cookie: { maxAge: 60000 }, 
                  secret: 'woot',
                  resave: false, 
                  saveUninitialized: false}));

app.use(express.static("css"));
app.use(express.static("imgs"));
app.use(express.static("js"));
app.use(flash());
app.use(express.urlencoded({extended: true}));
// app.use(express.json()); 

app.set("view engine", "ejs");



app.get("/", (req,res,next)=>{
    loadData().then(value => {
        res.render("home", {endorsements: value, messageE: req.flash("successE"), messageM: req.flash("successM")});
    }); 
});
app.get("/about", (req,res,next)=>{
    res.render("about");
});

app.get("/endorsement", (req,res,next)=>{
    res.render("endorsement");
});
app.get("/certificates", (req,res,next)=>{
    res.render("certificates");
});
app.get("/contact", (req,res,next)=>{
    res.render("contactpage");
});
app.get("/references", (req,res,next)=>{
    res.render("references");
});
app.post("/endorsement", (req,res,next)=>{
    let data = {
        name: req.body.name,
        profileURL: req.body.profileURL,
        designation: req.body.designation,
        company: req.body.company,
        words: req.body.words
    }
    endrsmnt.create(data, (err,val)=>{
        if(err){
            console.log(err);
        }
        else{
            req.flash("successE", "Your words are added successfully! Thank you for your precious time.")
            console.log(val);
            res.redirect("/");
        }
    })
});

app.post("/sendmail", (req,res)=>{
    let sentMail = req.body.sentMail;
    let mailOption1, mailOption2;

    ejs.renderFile("./views/templates/email_template.ejs",{sentmail: req.body.sentMail}, (err, data)=>{
        if(err){
            console.log(err);
        }else{
            mailOption1={
                from: "vermakash.1.3.0@gmail.com",
                to: "vermakash.1.3.0@gmail.com",
                subject: "From Manideep's Website",
                html: data
            }
            mailOption2={
                from: "vermakash.1.3.0@gmail.com",
                to: sentMail.email,
                subject: "Your email to Manideep",
                html: data
            }
        };
    })

    sendMail(mailOption1);
    if(sentMail.metoo === "true"){
        sendMail(mailOption2);
    }
    req.flash("successM", "Your email has sent successfully! I'll contact you soon.")
    res.redirect("/");
})

app.listen(process.env.PORT || 3000, ()=>{
    console.log("server started!");
})


var ctx = document.getElementById('myChart');
function makeChart(){
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Python', 'HTML', 'CSS', 'Javascript', 'Java', 'Nodejs', 'Git&Github', 'MongoDB', 'SQL', 'AWS', 'React'],
            datasets: [{
                label: 'Skillset(%)',
                data: [90, 85, 100, 75, 60, 80, 70, 65,50, 90, 95],
                backgroundColor: [
                    'rgb(22, 204, 131)', 
                    'rgb(82, 179, 90)',  
                    'rgb(61, 252, 147)', 
                    'rgb(141, 212, 83)', 
                    'rgb(82, 179, 137)', 
                    'rgb(138, 222, 104)', 
                    'rgb(77, 163, 129)', 
                    'rgb(80, 242, 194)', 
                    'rgb(47, 204, 50)',  
                    'rgb(132, 227, 155)', 
                    'rgb(82, 179, 137)' 
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    grid: {display:false},
                    ticks: {
                        color: 'rgba(#245f3f, 1)',
                        font: {
                            size: screen.width/60,
                            weight: 700
                        },
                        // autoSkip: false,
                        // maxRotation: 90,
                        // minRotation: 90,
                        // padding: -120,
                        // z: 50
                    }
                },
                x: {
                    grid:{display:false},
                    ticks: {
                        display: false 
                    }
                }    
            },
            indexAxis: 'y',
            responsive: true,
            labels:{
                font:{
                    size: 25
                }
            }
        }
    });
}

var inView = false;

function isScrolledIntoView(elem)
{
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();

    return ((elemTop <= docViewBottom) && (elemBottom >= docViewTop));
}

$(window).scroll(function() {
    if (isScrolledIntoView('#myChart')) {
        if (inView) { return; }
        inView = true;
        makeChart();
        
    } else {
        inView = false;  
    }
});
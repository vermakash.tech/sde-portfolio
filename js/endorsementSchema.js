var mongoose = require("mongoose");

var endorsementSchema = new mongoose.Schema({
    name: String,
    profileURL: String,
    designation: String,
    company: String,
    words: String
});

module.exports = mongoose.model("endorsement", endorsementSchema);